#include "the_scroll_area.h"
#include "tomeo.cpp"

TheScrollArea::TheScrollArea() {
    scroll = new QScrollArea(); // create scroll area
    // the scroll area is always vertical
    scroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    scroll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
    // set size of scroll area
    scroll->setWidgetResizable( true );
    scroll->setGeometry( 100, 100, 800, 680 );
    scroll->sizeHint();
    scroll->setMinimumWidth(300);
}
