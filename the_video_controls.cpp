#include "the_video_controls.h"

TheVideoControls::TheVideoControls(TheVideoQueue* video_queue,
    QWidget* parent) : QWidget(parent), video_queue_(video_queue) {
    // create video widget and media player
    QVideoWidget *videoWidget = new QVideoWidget();
    media_player_ = new ThePlayer();
    media_player_->setVideoOutput(videoWidget);

    // fix MacOS's widget size tantrum
    videoWidget->setMinimumWidth(400);
    videoWidget->setMinimumHeight(300);

    QBoxLayout* time_row = setupTimeRow(); // set up time row
    connect(media_player_, &QMediaPlayer::durationChanged, this,
            &TheVideoControls::durationChanged);
    connect(timeline_, &QAbstractSlider::sliderMoved, media_player_,
            &QMediaPlayer::setPosition);
    connect(media_player_, &QMediaPlayer::positionChanged, timeline_,
            &QAbstractSlider::setSliderPosition);
    connect(media_player_, &QMediaPlayer::positionChanged, this,
            &TheVideoControls::timePositionChanged);

    QBoxLayout* control_row = setupControlRow(); // setup control row
    connect(seek_back_, SIGNAL(clicked()), media_player_,
            SLOT(seek_back())); // move 5 seconds backwards
    connect(state_play_,SIGNAL(clicked()), media_player_,
            SLOT(play())); // plays the video
    connect(state_pause_,SIGNAL(clicked()), media_player_,
            SLOT(pause())); // pauses the video
    connect(state_stop_,SIGNAL(clicked()), media_player_,
            SLOT(stop())); // stops the video
    connect(seek_forward_,SIGNAL(clicked()), media_player_,
            SLOT(seek_forward())); // move 5 seconds forward
    connect(mute_, SIGNAL(clicked()), this,
            SLOT(toggleMute())); // mute the video
    connect(volume_slider_, SIGNAL(valueChanged(int)),media_player_,
            SLOT(setVolume(int))); // increase/decrease volume
    connect(volume_slider_, SIGNAL(valueChanged(int)), this,
            SLOT(volumeChanged(int))); // volume percentage
    connect(next_, SIGNAL(clicked()), this,
            SLOT(next())); // skips to next video in queue
    connect(previous_, SIGNAL(clicked()), this,
            SLOT(previous())); // skips to last video in queue


    QBoxLayout* video_column = new QVBoxLayout();
    video_column->addWidget(videoWidget);
    video_column->addLayout(time_row);
    video_column->addLayout(control_row);
    // TheVideoControls is a widget so use video_column layout as widget layout
    setLayout(video_column);
}

void TheVideoControls::volumeChanged(int percentage) {
    // set text of volume label to percentage/position of the volume_slider_ widget
    volume_->setText(QString::number(percentage) + "%");
}

void TheVideoControls::toggleMute() {
    //toggle mute
    is_muted_ = !is_muted_;

    if (is_muted_) {
        // store previous volume to return to once unmuted
        previous_volume_ = volume_slider_->sliderPosition();
        mute_->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
        volume_slider_->setSliderPosition(0);
        volume_slider_->setEnabled(false); // ensure volume slider can't change
    } else {
        // restore slider to previous state
        mute_->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
        volume_slider_->setEnabled(true);
        volume_slider_->setSliderPosition(previous_volume_);
    }
}

void TheVideoControls::durationChanged(int duration) {
    // timeline range should be updated if new video is loaded
    timeline_->setRange(0, duration);
}

void TheVideoControls::timePositionChanged(int position) {
    // create and display current position of video in minutes:seconds format
    QString time = TheVideoControls::positionToTimeStr(position)
            + "/" + TheVideoControls::positionToTimeStr(media_player_->duration());
    time_->setText(time);
}

void TheVideoControls::next() {
    // dequeue and play next video in queue
    video_queue_->playNext(media_player_);
}

void TheVideoControls::previous() {
    // enqueue video popped from history stack
    video_queue_->playPrevious();
}

QBoxLayout* TheVideoControls::setupTimeRow() {
    /*
     * the following segment of code creates the UI elements for
     * controlling the position of the video (the current time in the video)
     * time_ is a label representing video time 00:00 to video duration
     * timeline_ is a slider that allows the user to change video position
     * time_row is the horizontal layout that contains them
     */
    time_ = new QLabel();
    time_->setText("00:00");

    timeline_ = new QSlider(Qt::Horizontal);

    QBoxLayout* time_row = new QHBoxLayout();
    time_row->setMargin(0);
    time_row->addWidget(time_);
    time_row->addWidget(timeline_);
    return time_row;
}

QBoxLayout* TheVideoControls::setupControlRow() {
    // create seek and seek buttons
    seek_back_ = new QPushButton();
    seek_back_->setIcon(style()->standardIcon(QStyle::SP_MediaSeekBackward));
    seek_back_->setToolTip("Seek Back"); //display usability over the button
    state_play_ = new QPushButton();
    state_play_->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    state_play_->setToolTip("Play");
    state_pause_ = new QPushButton();
    state_pause_->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    state_pause_->setToolTip("Pause");
    state_stop_ = new QPushButton();
    state_stop_->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
    state_stop_->setToolTip("Stop");
    seek_forward_ = new QPushButton();
    seek_forward_->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    seek_forward_->setToolTip("Seek Forward");
    // create volume buttons
    mute_ = new QPushButton();
    mute_->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
    mute_->setToolTip("Mute");
    volume_slider_ = new QSlider(Qt::Horizontal);
    volume_slider_->setRange(0, 100);
    volume_ = new QLabel();
    volume_->setText("0%");
    //create queue buttons
    next_ = new QPushButton();
    next_->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    next_->setToolTip("Next Video");
    previous_ = new QPushButton();
    previous_->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
    previous_->setToolTip("Previous Video");

    // create control layout
    QBoxLayout* control_row = new QHBoxLayout();
    control_row->setMargin(0);
    control_row->addWidget(seek_back_);
    control_row->addWidget(state_play_);
    control_row->addWidget(state_pause_);
    control_row->addWidget(state_stop_);
    control_row->addWidget(seek_forward_);
    control_row->addWidget(mute_);
    control_row->addWidget(volume_slider_);
    control_row->addWidget(volume_);
    control_row->addWidget(previous_);
    control_row->addWidget(next_);
    return control_row;
 }

const QString TheVideoControls::positionToTimeStr(int position) {
    int minutes = (position / (60 * 1000)) % 60, // get minutes from ms
        seconds = (position / 1000) % 60; // get seconds from ms

    // if minutes or seconds are lower than 10, a 0 digit must be placed
    // to keep to the format of the time e.g. 05:05 instead of 5:5
    QString minutes_str = minutes < 10 ? "0" + QString::number(minutes)
                                       : QString::number(minutes),
            seconds_str = seconds < 10 ? "0" + QString::number(seconds)
                                       : QString::number(seconds);
    return minutes_str + ":" + seconds_str;
}
