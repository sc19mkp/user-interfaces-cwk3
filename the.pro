QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        the_button.cpp \
        the_player.cpp \
        the_scroll_area.cpp \
        the_video_controls.cpp \
        the_video_loader.cpp \
        the_video_queue.cpp \
        tomeo.cpp

HEADERS += \
    the_button.h \
    the_player.h \
    the_scroll_area.h \
    the_timeline.h \
    the_video_controls.h\
    the_video_loader.h \
    the_video_queue.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

