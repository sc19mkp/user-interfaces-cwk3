/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <string>
#include <vector>
#include <QApplication>
#include "the_player.h"
#include "the_button.h"
#include "the_video_controls.h"
#include "the_scroll_area.h"
#include "the_video_queue.h"
#include "the_video_loader.h"

using namespace std;

int main(int argc, char *argv[]) {
    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // create the main window
    QWidget window;
    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);

    // create and intialize video queue widget
    TheVideoQueue* video_queue_widget = new TheVideoQueue();
    // create and initialize video player widget with controls
    TheVideoControls* controls = new TheVideoControls(video_queue_widget);

    // create top widget for side bar
    QWidget *scrollbuttons = new QWidget();
    // create layout of the sidebar video buttons
    QVBoxLayout *layout2 = new QVBoxLayout();
    scrollbuttons->setLayout(layout2);

    // create loader and pass it necessary widgets / layouts
    TheVideoLoader* loader = new TheVideoLoader(controls->getPlayer(), layout2,
                                                video_queue_widget, scrollbuttons);
    loader->loadDefaultVideoFolder(); // load videos in default folder
    if (loader->numVideos() == 0) { // if no videos exist
        loader->loadCustomVideoFolder(); // let the user choose
        if (loader->numVideos() == 0) { // if no videos exist
            // we have no videos to start Tomeo with, exit!!!
            exit(-1);
        }
    }

    // create scroll area for side bar
    TheScrollArea scrolls;
    scrolls.scroll->setWidget(scrollbuttons);

    // create top layout for window
    QVBoxLayout *top = new QVBoxLayout();
    top->addWidget(controls); // add controls and video widget

    // create and add queue widget within horizontal scroll area
    QScrollArea* queue_scroll_area = new QScrollArea();
    queue_scroll_area->setMinimumHeight(175);
    queue_scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    queue_scroll_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    queue_scroll_area->setWidget(video_queue_widget);
    top->addWidget(queue_scroll_area);

    // the side bar scroll area should be arranged
    // horizontally with the rest of the window
    QHBoxLayout *layout3 = new QHBoxLayout();
    layout3->addLayout(top);

    // layout for side bar and load folder button
    QVBoxLayout* button_layout = new QVBoxLayout();
    // create new video loader button and ensure it does its job
    QAbstractButton* load_button = new QPushButton("Load Video Folder");
    load_button->connect(load_button, &QAbstractButton::clicked, loader,
            &TheVideoLoader::loadCustomVideoFolder);
    button_layout->addWidget(load_button);

    // attach the scroll area
    button_layout->addWidget(scrolls.scroll);
    layout3->addLayout(button_layout);

    // ensure layout of window is set
    window.setLayout(layout3);

    // showtime!
    window.show();

    return app.exec();
}
