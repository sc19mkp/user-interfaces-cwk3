#ifndef THE_VIDEO_LOADER_H
#define THE_VIDEO_LOADER_H

#include <QDialog>
#include <QMessageBox>
#include <QDir>
#include <QDirIterator>
#include <QFileDialog>
#include <QImageReader>
#include "the_player.h"
#include "the_video_queue.h"

class TheVideoLoader : public QWidget {
private:
    ThePlayer* media_player_;
    QLayout* layout_;

    TheVideoQueue* video_queue_widget_;
    QWidget* top_;

    vector<TheButtonInfo*> videos_;
    vector<TheButton*> buttons_;
public:
    TheVideoLoader(ThePlayer* media_player, QLayout* layout,
                   TheVideoQueue* queue, QWidget* top);

    /*
     * loadDefaultVideoFolder
     * loads the default folder
     *
     * - this is the the same directory as the code for
     *   git repo purposes -
     */
    void loadDefaultVideoFolder();

    /*
     * loadCustomVideoFolder
     * loads custom folder specified by user
     */
    void loadCustomVideoFolder();

    /*
     * numVideos
     * returns the number of videos loaded
     */
    unsigned numVideos() { return videos_.size(); }
private:
    /*
     * loadVideoFolder
     * resets application and loads new video folder. If the
     * folder contains no videos, it quits the application.
     */
    void loadVideoFolder(const QString path);

    /*
     * reset
     * clears video queue, removes buttons and videos to prepare
     * for new video folder to be loaded
     */
    void reset();

    /*
     * getInfoIn
     * retrieves video information and stores it in vector from
     * path loc
     */
    std::vector<TheButtonInfo*> getInfoIn(string loc);
};

#endif // THE_VIDEO_LOADER_H
