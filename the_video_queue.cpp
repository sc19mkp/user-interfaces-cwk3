#include "the_video_queue.h"


TheVideoQueue::TheVideoQueue(QWidget* parent) : QWidget(parent) {
    layout_ = new QHBoxLayout(this);
    layout_->setMargin(5); // make margin small
    layout_->setSpacing(5); // make spacing small
    // resize mode will be constrained by button max size
    layout_->setSizeConstraint(QLayout::SetMaximumSize);
}

void TheVideoQueue::playNext(ThePlayer* media_player) {
    // dequeue video in front of queue
    TheButtonInfo* next_video_ = deQueue();
    if (next_video_ != NULL) {
        // start playing the video
        media_player->jumpTo(next_video_);
    }
}

void TheVideoQueue::playPrevious() {
    // get video that is at the top of the stack
    TheButtonInfo* previous_video_ = prevQueue();
    if (previous_video_ != NULL) {
        // queue video on top of history stack
        enQueue(previous_video_);
    }
}

void TheVideoQueue::clear() {
    // ensure video queue is empty and memory is freed
    video_queue_.clear();
    video_queue_.shrink_to_fit();

    // ensure video queue is empty and memory is freed
    history_stack_.clear();
    history_stack_.shrink_to_fit();

    //resize widget to 0 width
    resize(0, 175);

    // delete all widgets that remain
    QList<QWidget *> widgets = findChildren<QWidget *>();
    foreach (QWidget * widget, widgets)
    {
        delete widget;
    }
}

void TheVideoQueue::enQueue(TheButtonInfo* info) {
    // create button for enqueued video
    TheButton* button = new TheButton(this);
    button->init(info);
    button->setMinimumWidth(250);
    button->setMinimumHeight(100);
    button->setEnabled(false);
    video_queue_.push_back(button);

    layout_->addWidget(button); // add to layout
    button->show();

    resize(video_queue_.size() * 260, 175);
}

TheButtonInfo* TheVideoQueue::deQueue() {
    // if video queue is not empty
    if (video_queue_.size() > 0) {
        // get button at the front, and remove it
        TheButton* front = video_queue_.front();
        front->setParent(0);
        layout_->removeWidget(front);
        video_queue_.pop_front();

        resize(video_queue_.size() * 260, 175);

        // ensure we put the previous video in history
        enQueueBack(front->info);
        delete front;

        return history_stack_.front();
    }

    return NULL;
}

void TheVideoQueue::enQueueBack(TheButtonInfo* button) {
    history_stack_.push_front(button); // place on top of stack
}

TheButtonInfo* TheVideoQueue::prevQueue() {
    if (history_stack_.size() != 0) {
        // remove and return top of stack
        TheButtonInfo *prev = history_stack_.front();
        history_stack_.pop_front();
        return prev;
    }

    return NULL;
}
