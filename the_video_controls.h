#ifndef THE_VIDEO_CONTROLS_H
#define THE_VIDEO_CONTROLS_H

#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QStyle>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QVideoWidget>
#include <QLabel>
#include <QtWidgets>
#include "the_video_queue.h"

using namespace std;

/*
 * TheVideoControls
 * creates a ThePlayer object and control buttons/sliders
 * to control the video that is being played
 */
class TheVideoControls : public QWidget {
    Q_OBJECT
private:
    TheVideoQueue* video_queue_;
    ThePlayer* media_player_;
    QLabel* time_,
        * volume_;
    QAbstractButton* state_play_,
        * seek_back_,
        * seek_forward_,
        * next_,
        * previous_,
        * state_pause_,
        * state_stop_,
        * mute_;
    QComboBox* rate_box_;
    QAbstractSlider* timeline_,
        * volume_slider_;

    bool is_muted_ = false;
    int previous_volume_ = 0;
public:
    explicit TheVideoControls(TheVideoQueue* video_queue, QWidget* parent = nullptr);

    /*
     * getPlayer
     * return a ThePlayer object so tomeo.cpp can call ThePlayer::setContent
     */
    ThePlayer* getPlayer() { return media_player_; }
private slots:
    /*
     * volumeChanged
     * changes volume_ label's text when volume_slider_ changes
     */
    void volumeChanged(int percentage);

    /*
     * toggleMute
     * mutes the video and disables volume_slider_
     */
    void toggleMute();

    /*
     * durationChanged
     * changes range of timeline_ slider when video is changed
     */
    void durationChanged(int duration);

    /*
     * timePositionChanged
     * changes time_ label's text when position is changed
     */
    void timePositionChanged(int position);

    /*
     * next
     * skip to next video in the queue
     */
    void next();

    /*
     * previous
     * queue previous played video
     */
    void previous();
private:
    /*
     * setupTimeRow
     * creates layout for time control row
     */
    QBoxLayout* setupTimeRow();

    /*
     * setupControlRow
     * creates layout for video control row (volume, state, position in queue)
     */
    QBoxLayout* setupControlRow();

    /*
     * positionToTimeStr
     * takes time in ms and converts to mm:ss string format
     */
    static const QString positionToTimeStr(int position);
};

#endif // THE_VIDEO_CONTROLS_H
