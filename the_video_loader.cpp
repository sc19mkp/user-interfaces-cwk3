#include "the_video_loader.h"

std::vector<TheButtonInfo*> TheVideoLoader::getInfoIn(string loc) {
    std::vector<TheButtonInfo*> out;
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files
        QString f = it.next();
        QFileInfo metadata(f);
        QString name = metadata.fileName();
        QString birth_date = metadata.birthTime().toString();

        if (f.contains("."))

#if defined(_WIN32)
        if (f.contains(".wmv"))  { // windows
#else
        if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                         // voodoo to create an icon for the button
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite));
                         // convert the file location to a generic url
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f ));
                         // add to the output list
                        out.push_back(new TheButtonInfo(url, ico, name, birth_date));
                    }
                    else
                        qDebug() << "warning: skipping video because I"
                                    " couldn't process thumbnail " << thumb << Qt::endl;
            }
            else
                qDebug() << "warning: skipping video because"
                            " I couldn't find thumbnail " << thumb << Qt::endl;
        }
    }

    return out;
}

TheVideoLoader::TheVideoLoader(ThePlayer* media_player, QLayout* layout,
               TheVideoQueue* queue, QWidget* top)
    : QWidget(nullptr), media_player_(media_player), layout_(layout),
      video_queue_widget_(queue), top_(top) {
}

void TheVideoLoader::loadDefaultVideoFolder() {
    // load folder from git repo directory
    loadVideoFolder("../user-interfaces-cwk3/videos/");
}

void TheVideoLoader::loadCustomVideoFolder() {
    // use file dialog to load new video folder
    QString file_dir = QFileDialog::getExistingDirectory(NULL,
                        "Tomeo: Select a folder of videos to open.", ".");
    loadVideoFolder(file_dir);
}

void TheVideoLoader::loadVideoFolder(const QString path) {
    std::vector<TheButtonInfo*> videos = getInfoIn(path.toStdString());
    if (videos.size() == 0) {
        // display error and return so old videos remain
        QMessageBox::critical(NULL, "Tomeo Error: No videos available",
                              "Please choose a valid video folder.");
        return;
    }

    reset(); // reset application - remove videos and buttons
    videos_ = std::vector<TheButtonInfo*>(videos); // copy videos

    // create and instantiate buttons for side panel
    for (unsigned i = 0; i < videos_.size(); i++) {
        TheButton *vid = new TheButton(top_);
        // queue video when clicked
        vid->connect(vid, SIGNAL(jumpTo(TheButtonInfo* )),
                        video_queue_widget_,
                        SLOT (enQueue(TheButtonInfo* )));
        buttons_.push_back(vid);
        layout_->addWidget(vid);
        vid->init(videos_[i]);
    }

    // ensure the content is set for the media player
    media_player_->setContent(&buttons_, &videos_);

    // we must queue our first video and play it for queue/history stack consistency
    video_queue_widget_->enQueue(videos_[0]);
    video_queue_widget_->playNext(media_player_);
}

void TheVideoLoader::reset() {
    video_queue_widget_->clear(); // clear video queue
    if (videos_.size() > 0) {
        // delete all TheButtonInfos as new videos are going to be loaded
        for (unsigned i = 0; i < videos_.size(); i++) {
           delete videos_[i];
        }

        // clear and shrink TheButtonInfo vector
        videos_.clear();
        videos_.shrink_to_fit();
    }

    if (buttons_.size() > 0) {
        //ensure all buttons are removed and deleted
        for (unsigned i = 0; i < buttons_.size(); i++) {
            layout_->removeWidget(buttons_[i]);
            buttons_[i]->setParent(0);
            delete buttons_[i];
        }
        // clear and shrink TheButton vector
        buttons_.clear();
        buttons_.shrink_to_fit();
    }
}
