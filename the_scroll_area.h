#ifndef SCROLLAREA_H
#define SCROLLAREA_H

#include <QtWidgets>

/*
 * TheScrollArea
 * contains the scroll area for the sidebar
 * of video button widgets
 */
class TheScrollArea {
public:
    TheScrollArea();
    QScrollArea* scroll;
};

#endif // SCROLLAREA_H
