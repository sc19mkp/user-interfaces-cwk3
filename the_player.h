//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H

#include <iostream>
#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>

using namespace std;

class ThePlayer : public QMediaPlayer {
    Q_OBJECT
private:
    vector<TheButtonInfo*>* infos;
    vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        mTimer = new QTimer(NULL);
        mTimer->setInterval(1000); // 1000ms is one second between ...
        mTimer->start();
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo*>* i);

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);
    void seek (int seconds) {setPosition(seconds*1000);}
    void seek_back(int seconds=5) { //move backwards by 5 seconds
        setPosition(position() - seconds*1000);
    }
    void seek_forward(int seconds=5){ //move forward by 5 seconds
        setPosition(position() + seconds*1000);
    }
};

#endif //CW2_THE_PLAYER_H
