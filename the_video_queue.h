#ifndef THE_VIDEO_QUEUE_H
#define THE_VIDEO_QUEUE_H

#include <deque>
#include <QWidget>
#include <QHBoxLayout>
#include "the_button.h"
#include "the_player.h"

using namespace std;

/*
 * TheVideoQueue
 * this class provides a queue widget which allows the queueing
 * of videos (or requeuing of previously watched videos) which
 * works using TheVideoControls's next and previous buttons.
 */
class TheVideoQueue : public QWidget {
    Q_OBJECT
private:
    std::deque<TheButton*> video_queue_; // video queue
    std::deque<TheButtonInfo*> history_stack_; // history stack

    QLayout* layout_; // layout of widget
public:
    explicit TheVideoQueue(QWidget* parent = nullptr);

    /*
     * playNext
     * deQueues front of queue and plays the video specified by
     * TheButtonInfo::url
     *
     * - used within TheVideoControls to skip to next in the queue -
     */
    void playNext(ThePlayer* media_player);

    /*
     * playPrevious
     * takes previously deQueued video on top of stack and queues it again
     */
    void playPrevious();

    /*
     * clear
     * clears the stack and queue so new videos can be loaded
     */
    void clear();

public slots:
    /*
     * enQueue
     * adds a button to the back of the queue initialized using info, then adds
     * the button to layout_ so it can be displayed in the horizontal bar
     * at the bottom of the window
     *
     * - this must be called to add a video to the queue -
     */
    void enQueue(TheButtonInfo* info);
private:
    /*
     * deQueue
     * removes a button from the front of the queue and from layout_
     * so it is no longer displayed in the bottom bar
     *
     * - the result is used in playNext, hence it is a private method -
     */
    TheButtonInfo* deQueue();

    /*
     * enQueueBack
     * puts the previously played video on the history stack
     */
    void enQueueBack(TheButtonInfo* button);

    /*
     * prevQueue
     * returns the top of the stack, which is the previous video in the queue
     */
    TheButtonInfo* prevQueue();

};

#endif // THE_VIDEO_QUEUE_H
