//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H

#include <iostream>
#include <QToolButton>
#include <QUrl>


class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display
    QString name, // name of the file
            birth_date; // when file was created

    TheButtonInfo ( QUrl* url, QIcon* icon, QString name,
                    QString birth_date)
        : url (url), icon (icon), name(name), birth_date(birth_date) {}
};

class TheButton : public QToolButton {
    Q_OBJECT

public:
    TheButtonInfo* info;

     TheButton(QWidget *parent) :  QToolButton(parent) {
         setIconSize(QSize(200,110));
         // if QPushButton clicked...then run clicked() below
         connect(this, SIGNAL(released()), this, SLOT (clicked()));
    }

    void init(TheButtonInfo* i);

private slots:
    void clicked();

signals:
    void jumpTo(TheButtonInfo*);

};

#endif //CW2_THE_BUTTON_H
