//
// Created by twak on 11/11/2019.
//

#include "the_button.h"

void TheButton::init(TheButtonInfo* i) {
    setIcon( *(i->icon) );
    info =  i;

    setToolButtonStyle(Qt::ToolButtonTextUnderIcon); // icon above text
    setText(i->name + ", created " + i->birth_date); // set button text
}

void TheButton::clicked() {
    emit jumpTo(info);
}
